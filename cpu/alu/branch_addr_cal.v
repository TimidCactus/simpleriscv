module branch_addr_cal (
	input		wire								rst,

	input		wire[`INSTADDRBUS]	pc,

	input		wire[`OPBUS]				opcode,
	input		wire[`FUNC3BUS]			func3,

	input		wire[`REGBUS]				s1,
	input		wire[`REGBUS]				s2,
	input		wire[`REGBUS]				imm,

	output	reg									branch_flag,
	output	reg[`INSTADDRBUS]		branch_addr
);

	always @ (*) begin
		case(opcode)
			`OP_BRANCH	:begin
				case(func3)
					`FUNC3_BEQ	: begin
						if(s1 == s2)	begin
							branch_addr	<= pc + imm;
							branch_flag	<= `BRANCH;
						end else begin
							branch_flag	<= `NOTBRANCH;
						end
					end
					`FUNC3_BNE	:
						if(s1 != s2)	begin
							branch_addr	<= pc + imm;
							branch_flag	<= `BRANCH;
						end else begin
							branch_flag	<= `NOTBRANCH;
						end
					end
					`FUNC3_BLT	: begin
						if(s1[31] == s2[31]) begin
							if(s1 < s2) begin
								branch_addr	<= pc + imm;
								branch_flag	<= `BRANCH;
							end else begin
								branch_flag	<= `NOTBRANCH;
							end
						end else begin
							if(s1 == 1'b1) begin
								branch_addr	<= pc + imm;
								branch_flag	<= `BRANCH;
							end else begin
								branch_flag	<= `NOTBRANCH;
							end
						end
					end
					`FUNC3_BGE	:
						if(s1[31] == s2[31]) begin
							if(s1 >= s2) begin
								branch_addr	<= pc + imm;
								branch_flag	<= `BRANCH;
							end else begin
								branch_flag	<= `NOTBRANCH;
							end
						end else begin
							if(s1 == 1'b0) begin
								branch_addr	<= pc + imm;
								branch_flag	<= `BRANCH;
							end else begin
								branch_flag	<= `NOTBRANCH;
							end
						end
					end
					`FUNC3_BLTU	: begin
						if(s1 < s2) begin
							branch_addr	<= pc + imm;
							branch_flag	<= `BRANCH;
						end else begin
							branch_flag	<= `NOTBRANCH;
						end
					end
					`FUNC3_BGEU	: begin
						if(s1 >= s2) begin
							branch_addr	<= pc + imm;
							branch_flag	<= `BRANCH;
						end else begin
							branch_flag	<= `NOTBRANCH;
						end
					end
			end
			`OP_JAL			: begin
				branch_addr	<= pc + imm;
				branch_flag	<= `BRANCH;
			end
			`OP_JALR		: begin
				branch_addr <= s1 + imm;
				branch_flag	<= `BRANCH;
			end
			default			: begin
				branch_flag	<= `NOTBRANCH;
			end
	end

endmodule
