module inst_rom(
    input   wire[`INSTADDRBUS]  addr,
    output  reg[`INSTBUS]       inst
);
    reg[`INSTBUS]   inst_mem[0:`DATAMEMNUM];

    initial $readmemh ("inst.txt",inst_mem);

    always @ (*) begin
        inst <= inst_mem[addr[`INSMEMWIDTH+1:2]];
    end
endmodule
