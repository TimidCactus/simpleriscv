module ins_mem(
	input		wire								en,
	input		wire[`INSADDRBUS]		addr,
	output	reg[`INSTBUS]				inst
);
	reg[`INSTBUS] inst_mem[0:`INSMEMNUM];
	initial $readmemh("inst_rom.data",inst_mem);
	assign inst = inst_mem[addr[`INSMEMWIDTH+1:2]] & {`INSBUS{en}};
endmodule
