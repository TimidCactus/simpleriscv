module inst_decode (
	input		wire								rst,

	input		wire[`INSTBUS]			inst_i,

	output	reg[`OPBUS]					opcode,
	output	reg[`FUNC3BUS]			func3,
	output	reg[`FUNC7BUS]			func7,
	output	reg[`REGADDRBUS]		s1_addr,
	output	reg[`REGADDRBUS]		s2_addr,
	output	reg[`REGADDRBUS]		des_addr,
	output	reg[`REGBUS]				imm
);

	assign opcode = inst_i[6:0];
	assign fun3 = inst[14:12];
	assign fun7 = inst[31:25];
	assign s1_addr = inst[19:15];
	assign s2_addr = inst[24:20]; //shift imm
	assign des_addr = inst[11:7];
	always @ (*) begin
		case(opcode) 
			`OP_OP_IMM		: imm <= {20{inst[31]},inst[31:20]};//I
			`OP_BRANCH		: imm <= {21{inst[31]},inst[7],inst[30:25],inst[11:8],1'b0};//SB
			`OP_LUI				: imm <= {inst[31:12],12'b000000000000};//U
			`OP_AUIPC			: imm <= {inst[31:12],12'b000000000000};//U
			`OP_JAL				:	imm <= {12{inst[31]},inst[19:12],inst[20],inst[30:21],1'b0};//UJ
			`OP_JALR			: imm <= {20{inst[31]},inst[31:20]};//I
			`OP_LOAD			: imm <= {20{inst[31]},inst[31:20]};//I
			`OP_STORE			: imm <= {21{inst[31]},inst[30:25],inst[11:7]};//S
			`OP_MISC_MEM	: imm <= {20{inst[31]},inst[31:20]};//I
			default				: imm <= `ZEROWORD;//R
		endcase
	end

endmodule
