module data_mem (
	input		wire								clk,
	input		wire								en,

	input		wire								we,
	input		wire[`MEMSELBUS]		sel,
	input		wire[`REGADDRBUS]		addr,
	input		wire[`REGBUS]				data_i,

	output	wire[`REGBUS]				data_o
);

	reg[`DATAWIDTH]		datamem0[0:`DATAMEMNUM-1];
	reg[`DATAWIDTH]		datamem1[0:`DATAMEMNUM-1];
	reg[`DATAWIDTH]		datamem2[0:`DATAMEMNUM-1];
	reg[`DATAWIDTH]		datamem3[0:`DATAMEMNUM-1];

	always @ (posedge clk) begin
		if(we == `WRITEENABLE  &&  en == `ENABLE) begin
			if(sel[0] == 1'b1) datamem0[addr[`datamemnumlog+1:2]]	<= data_i[7:0];
			if(sel[1] == 1'b1) datamem1[addr[`datamemnumlog+1:2]]	<= data_i[15:8];
			if(sel[2] == 1'b1) datamem2[addr[`datamemnumlog+1:2]]	<= data_i[23:16];
			if(sel[3] == 1'b1) datamem3[addr[`datamemnumlog+1:2]]	<= data_i[31:24];
		end
	end

	always @ (*) begin
		if(re == `READDISABLE) begin
			data_o	<= `ZEROWORD;
		end else begin
			data_o	<= {datamem3[addr[`datamemlog+1:2]],
									datamem2[addr[`datamemlog+1:2]],
									datamem1[addr[`datamemlog+1:2]],
									datamem0[addr[`datamemlog+1:2]]};
		end
	end

endmodule
