module ex_mem(
	input		wire								clk,
	input		wire								rst,

	input		wire[`OPBUS]				ex_opcode,
	input		wire[`FUNC3]				ex_fun3,

	input		wire[`REGBUS]				ex_result,

	input		wire[`REGADDRBUS]		ex_s2,
	input		wire[`REGADDRBUS]		ex_des_addr,
	input		wire[`REGBUS]				ex_wmdata,

	output	reg[`OPBUS]					mem_opcode,
	output	reg[`FUNC3]					mem_fun3,

	output	reg[`REGBUS]				mem_result,

	output	reg[`REGADDRBUS]		mem_s2,
	output	reg[`REGADDRBUS]		mem_des_addr,
	output	reg[`REGBUS]				mem_wmdata
);

	always @ (posedge clk) begin
		if(rst == `RSTENABLE) begin
			mem_opcode		<= `OP_ZERO;
			mem_func3			<= `FUNC3_ZERO;
			mem_result		<= `ZEROWORD;
			mem_s2				<= `ZEROWORD'
			mem_des_addr	<= `ZEROWORD;
			mem_wmdata		<= `ZEROWORD;
		end else begin
			mem_opcode		<= ex_opcode;
			mem_func3			<= ex_func3;
			mem_result		<= ex_result;
			mem_s2				<= ex_s2;
			mem_des_addr	<= ex_des_addr;
			mem_wmdata		<= ex_wmdata;
		end
	end

endmodule
