odule mem_controller (
    input        wire                                rst,

    input        wire[`OPBUS]                opcode,
    input        wire[`FUNC3BUS]            func3,

    input        wire[`REGBUS]                reg2,
    input        wire[`REGBUS]                mem_addr_i,
    input        wire[`REGBUS]                mem_data_i,

    output    reg                             mem_we,
    output    reg[`MEMSELBUS]            mem_sel,
    output    reg[`REGBUS]                mem_addr_o,
    output    reg[`REGBUS]                mem_data_o,
    output      reg[`REGBUS]            mem_write_o
);

    assign  mem_addr_o    = mem_addr_i;

    always @ (*) begin
        if(rst == `RSTENABLE) begin
            mem_we        = `WRITEDISABLE;
            mem_sel     = 4'b0000;
            mem_addr_o  = `ZEROWORD;
            mem_data_o  = `ZEROWORD;
        end else begin
            case(opcode)
                `OP_LOAD    : begin
                    mem_we            = `WRITEDISABLE;
                    case(func3)
                        `FUNC3_LB        : begin
                            case(mem_addr_i[1:0])
                                2'b00: begin
                                    mem_data_o    <= {{24{mem_data_i[31]}},mem_data_i[31:24]};
                                    mem_sel            <= 4'b1000;
                                end
                                2'b01: begin
                                    mem_data_o    <= {{24{mem_data_i[23]}},mem_data_i[23:16]};
                                    mem_sel            <= 4'b0100;
                                end
                                2'b10: begin
                                    mem_data_o    <= {{24{mem_data_i[15]}},mem_data_i[15:8]};
                                    mem_sel            <= 4'b0010;
                                end
                                2'b11: begin
                                    mem_data_o    <= {{24{mem_data_i[7]}},mem_data_i[7:0]};
                                    mem_sel            <= 4'b0001;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                            endcase
                        end
                        `FUNC3_LH        : begin
                            case(mem_addr_i[1:0])
                                2'b00    : begin
                                    mem_data_o    <= {{16{mem_data_i[31]}},mem_data_i[31:16]};
                                    mem_sel            <= 4'b1100;
                                end
                                2'b10    : begin
                                    mem_data_o    <= {{16{mem_data_i[15]}},mem_data_i[15:0]};
                                    mem_sel            <= 4'b0011;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                            endcase
                        end
                        `FUNC3_LW        : begin
                            mem_data_o    <= mem_addr_i;
                            mem_sel            <= 4'b1111;
                        end
                        `FUNC3_LBU    : begin
                            case(mem_addr_i[1:0])
                                2'b00: begin
                                    mem_data_o    <= {{24{1'b0}},mem_data_i[31:24]};
                                    mem_sel            <= 4'b1000;
                                end
                                2'b01: begin
                                    mem_data_o    <= {{24{1'b0}},mem_data_i[23:16]};
                                    mem_sel            <= 4'b0100;
                                end
                                2'b10: begin
                                    mem_data_o    <= {{24{1'b0}},mem_data_i[15:8]};
                                    mem_sel            <= 4'b0010;
                                end
                                2'b11: begin
                                    mem_data_o    <= {{24{1'b0}},mem_data_i[7:0]};
                                    mem_sel            <= 4'b0001;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                            endcase
                        end
                        `FUNC3_LHU    : begin
                            case(mem_addr_i[1:0])
                                2'b00    : begin
                                    mem_data_o    <= {{16{1'b0}},mem_data_i[31:16]};
                                    mem_sel            <= 4'b1100;
                                end
                                2'b10    : begin
                                    mem_data_o    <= {{16{1'b0}},mem_data_i[15:0]};
                                    mem_sel            <= 4'b0011;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                            endcase
                        end
                        default       :
                            mem_data_o    <= `ZEROWORD;
                    endcase
                end
                `OP_STORE    :    begin
                    mem_we            = `WRITEENABLE;
                    case(func3)
                        `FUNC3_SB        :  begin
                            mem_write_o <=  {4{reg2[7:0]}};
                            case(mem_addr_i[1:0])
                                2'b00:  begin
                                    mem_sel     <=  4'b1000;
                                end
                                2'b01:  begin
                                    mem_sel     <=  4'b0100;
                                end
                                2'b10:  begin
                                    mem_sel     <=  4'b0010;
                                end
                                2'b11:  begin
                                    mem_sel     <=  4'b0001;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                        `FUNC3_SH        :  begin
                            mem_write_o <=  {2{reg2[15:0]}};
                            case(mem_addr_i[1:0])
                                2'b00:  begin
                                    mem_sel     <=  4'b1100;
                                end
                                2'b11:  begin
                                    mem_sel     <=  4'b0011;
                                end
                                default:    mem_data_o    <= `ZEROWORD;
                            end
                        `FUNC3_SW        :  begin
                            mem_write_o <=  reg2;
                            mem_sel     <=  4'b1111;
                        end
                        default          :
                    endcase
                end
                default        :
            endcase
        end
    end

endmodule;
