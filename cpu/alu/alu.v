module alu(
	input		wire								rst,

	input		wire[`INSTADDRBUS]	pc,

	input		wire[`OPBUS]				opcode,
	input		wire[`FUNC3BUS]			func3,
	input		wire[`FUNC7BUS]			func7,

	input		wire[`REGBUS]				operand1,
	input		wire[`REGBUS]				operand2,
	input		wire[`REGBUS]				imm,

	output	reg[`REGBUS]				result
);

	always @ (*) begin
		case(opcode)
			`OP_OP				: begin
				case({func7,func3})
					{`FUNC7_ADD		,`FUNC3_ADD		}	: begin 
						result	<=	operand1 + operand2;
					end
					{`FUNC7_SUB		,`FUNC3_SUB		}	: begin
						result	<=	operand1 - operand2;
					end
					{`FUNC7_SLL		,`FUNC3_SLL		}	: begin
						result	<=	operand1 << operand2[4:0];
					end
					{`FUNC7_SLT		,`FUNC3_SLT		}	: begin
						if(operand1[31] == `POS  &&  operand2[31] == `NEG) result	<=	32'h00000000;
						if(operand1[31] == `NEG  &&  operand2[31] == `POS) result	<=	32'h00000001;
						if(operand1[31] == operand2[31] ) result	<=	{31{1'b0},{operand1 < operand2}};
					end
					{`FUNC7_SLTU	,`FUNC3_SLTU	}	: begin  
						result	<=	{31{1'b0},{operand1 < operand2}};
					end
					{`FUNC7_XOR		,`FUNC3_XOR		}	: begin
						result	<=	operand1 ^ operand2;
					end
					{`FUNC7_SRL		,`FUNC3_SRL		}	: begin
						result	<=	operand1 >> operand2[4:0];
					end
					{`FUNC7_SRA		,`FUNC3_SRA		}	: begin
						result	<=	({32{operand1[31]} << ( 6'b100000 - {1'b0,operand2[4:0]} )) | (operand1 >> operand2[4:0]);
					end
					{`FUNC7_OR		,`FUNC3_OR		}	: begin
						result	<=	operand1 | operand2;
					end
					{`FUNC7_AND		,`FUNC3_AND		}	: begin
						result	<=	operand1 & operand2;
					end
					default												: begin
						result	<=	`ZEROWORD;
					end
			 end 
			`OP_OP_IMM		: begin
				case(func3)
					`FUNC3_ADDI		: begin
						result	<=	operand1 + imm;
					end
					`FUNC3_SLLI		: begin
						result	<=	operand1 << imm[4:0];
					end
					`FUNC3_SLTI		: begin
						if(operand1[31] == `POS  &&  imm[31] == `NEG) result	<=	32'h00000000;
						if(operand1[31] == `NEG  &&  imm[31] == `POS) result	<=	32'h00000001;
						if(operand1[31] == imm[31] ) result	<=	{31{1'b0},{operand1 < imm}};
					end
					`FUNC3_SLTIU	: begin
						result	<=	{31{1'b0},{operand1 < imm}};
					end
					`FUNC3_XORI		: begin
						result	<=	operand1 ^ imm;
					end
					`FUNC3_SRXI		: begin
						if(imm[10] == 1'b0)	result	<=	operand1 >> imm[4:0];
						else	result	<=	({32{operand1[31]} << ( 6'b100000 - {1'b0,imm[4:0]} )) | (operand1 >> imm[4:0]);
					end
					`FUNC3_ORI		: begin
						result	<=	operand1 | imm;
					end
					`FUNC3_ANDI		: begin
						result	<=	operand1 & imm;
					end
					default				: begin
						result	<=	`ZEROWORD;
					end
			end
			`OP_BRANCH		: begin
				case(func3)
					`FUNC3_BEQ	: begin end
					`FUNC3_BNE	: begin end
					`FUNC3_BLT	: begin end
					`FUNC3_BGE	: begin end
					`FUNC3_BLTU	: begin end
					`FUNC3_BGEU	: begin end
					default			: begin end
			end
			`OP_LUI				: begin
				result	<=	imm;
			end
			`OP_AUIPC			: begin
				result	<=	imm + pc;
			end
			`OP_JAL				: begin
				result	<=	pc + 4;
			end
			`OP_JALR			: begin
				result	<=	pc + 4;
			end
			`OP_LOAD			: begin
				result	<=	operand1 + imm;
			end
			`OP_STORT			: begin
				result	<=	operand1 + imm;
			end
			`OP_MISC_MEM	: begin
				result	<=	imm;
			end
			default				: begin
				result	<= `ZEROWORD;
			end
	end

endmodule
