module id_ex(
	input		wire								clk,
	input		wire								rst,

	input		wire[`INSTADDRBUS]	id_pc,

    input       wire                id_delayslot,

	input		wire[`OPBUS]				id_opcode,
	input		wire[`FUNC3BUS]			id_func3,
	input		wire[`FUNC7BUS]			id_func7,

    input       wire[`REGADDRBUS]           id_operand1_addr,
	input		wire[`REGBUS]				id_operand1,
    input       wire[`REGADDRBUS]           id_operand2_addr,
	input		wire[`REGBUS]				id_operand2,

	input		wire[`REGADDRBUS]		id_des_addr,

	input		wire[`REGBUS]				id_imm,

    input       wire                id_we,

	output	reg[`INSTADDRBUS]	ex_pc,

    output  reg                     ex_delayslot,

	output	reg[`OPBUS]					ex_opcode,
	output	reg[`FUNC3BUS]			ex_func3,
	output	reg[`FUNC7BUS]			ex_func7,

    output  reg[`REGADDRBUS]        ex_operand1_addr,
    output	reg[`REGBUS]				ex_operand1,
    output  reg[`REGADDRBUS]        ex_operand2_addr,
	output	reg[`REGBUS]				ex_operand2,

	output	reg[`REGADDRBUS]		ex_des_addr,

	output	reg[`REGBUS]				ex_imm,

    output  reg                     ex_we
);

	always @ (posedge clk) begin
		if(rst == `RSTENABLE) begin
			ex_pc				<= `ZEROWORD;
            ex_delayslot        <= `DISDELAYSLOT;
			ex_opcode		<= `OP_ZERO;
			ex_func3		<= `FUNC3_ZERO;
			ex_func7		<= `FUNC7_ZERO;
			ex_operand1	<= `ZEROWORD;
            ex_operand1_addr    <= `ZEROADDR;
			ex_operand2	<= `ZEROWORD;
            ex_operand2_addr    <= `ZEROADDR;
			ex_des_addr	<= `ZEROADDR;
			ex_imm			<= `ZEROWORD;
            ex_we           <=  `WRITEDISABLE;
		end else begin
			ex_pc				<= id_pc;
            ex_delayslot        <= id_delayslot;
			ex_opcode		<= id_opcode;
			ex_func3		<= id_func3;
			ex_func7		<= id_func7;
			ex_operand1	<= id_operand1;
			ex_operand1_addr	<= id_operand1_addr;
			ex_operand2	<= id_operand2;
			ex_operand2_addr	<= id_operand2_addr;
			ex_des_addr	<= id_des_addr;
			ex_imm			<= id_imm;
            ex_we               <=  id_we;
		end
	end

endmodule
