module mem_wb (
	input		wire								clk,
	input		wire								rst,

	input		wire								mem_we,
	input		wire[`REGADDRBUS]		mem_des_addr,
	input		wire[`REGBUS]				mem_wmdata,

	output	reg									wb_we,
	output	reg[`REGADDRBUS]		wb_des_addr,
	output	reg[`REGBUS]				wb_wmdata
);

	always @ (posedge clk) begin
		if(rst == `rstenable) begin
			mem_we				<= 1'b0;
			mem_des_addr	<= `ZEROWORD;
			mem_wmdata		<= `ZEROWORD;
		end else begin
			wb_we				<= mem_we;
			wb_des_addr	<= mem_des_addr;
			wb_wmdata		<= mem_wmdata;
		end
	end

endmodule
