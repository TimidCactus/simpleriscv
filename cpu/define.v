
`define RSTENABLE				1'b1
`define RSTDISABLE			1'b0
`define WRITEENABLE			1'b1
`define WRITEDISABLE		1'b0
`define READENABLE			1'b1
`define READDISABLE			1'b0
`define ENABLE					1'b1
`define DISABLE					1'b0
`define POS							1'b0
`define NEG							1'b1
`define BRANCH					1'b1
`define NOTBRANCH				1'b0
`define ISDELAYSLOT            1'b1
`define NOTDELAYSLOT            1'b0
`define ZEROWORD				32'h00000000

// MEMORY
// INSTRUCTION 
`define INSTADDRBUS 		31:0
`define INSTBUS 				31:0
`define INSMEMNUM				131071
`define INSMEMWIDTH			17
`define MEMSELBUS				3:0
// DATA
`define DATAWIDTH			7:0
`define DATAADDRBUS		31:0
`define DATABUS				31:0
`define DATAMEMNUM			131071
`define DATAMEMLOG			17

// GENERAL REGISTERS
`define REGADDRBUS			4:0
`define REGBUS					31:0
`define REGADDRWIDTH		5
`define REGWIDTH				32
`define	ZEROADDR				5'b00000

// INSTRUCTION
`define OPBUS						6:0
`define OP_ZERO					7'b0000000
`define OP_OP						7'b0110011
`define OP_OP_IMM				7'b0010011
`define OP_BRANCH				7'b1100011
`define OP_LUI					7'b0110111
`define OP_AUIPC				7'b0010111
`define OP_JAL					7'b1101111
`define OP_JALR					7'b1100111
`define OP_LOAD					7'b0000011
`define OP_STORE				7'b0100011
`define OP_MISC_MEM			7'b0001111

`define FUNC3BUS				2:0
//OP
`define FUNC3_ZERO			3'b000
`define FUNC3_ADD				3'b000
`define FUNC3_SUB				3'b000
`define FUNC3_SLL				3'b001
`define FUNC3_SLT				3'b010
`define FUNC3_SLTU			3'b011
`define FUNC3_XOR				3'b100
`define FUNC3_SRL				3'b101
`define FUNC3_SRA				3'b101
`define FUNC3_OR				3'b110
`define FUNC3_AND				3'b111
//op_imm
`define FUNC3_ADDI			3'b000
`define FUNC3_SLLI			3'b001
`define FUNC3_SLTI			3'b010
`define FUNC3_SLTIU			3'b011
`define FUNC3_XORI			3'b100
`define FUNC3_SRXI			3'b101 // srli srai
`define FUNC3_ORI				3'b110
`define FUNC3_ANDI			3'b111
//branch
`define FUNC3_BEQ				3'b000
`define FUNC3_BNE				3'b001
`define FUNC3_BLT				3'b100
`define FUNC3_BGE				3'b101
`define FUNC3_BLTU			3'b110
`define FUNC3_BGEU			3'b111
//jalr
`define FUNC3_JALR			3'b000
//load
`define FUNC3_LB				3'b000
`define FUNC3_LH				3'b001
`define FUNC3_LW				3'b010
`define FUNC3_LBU				3'b100
`define FUNC3_LHU				3'b101
//store
`define FUNC3_SB				3'b000
`define FUNC3_SH				3'b001
`define FUNC3_SW				3'b010
`define FUNC3_SBU				3'b100
`define FUNC3_SHU				3'b101
//misc mem
`define FUNC3_FENCE			3'b000
`define FUNC3_FENCE_I		3'b001

`define FUNC7BUS				6:0
`define FUNC7_ZERO			7'b0000000
`define FUNC7_ADD				7'b0100000
`define FUNC7_SUB				7'b0000000
`define FUNC7_SLL				7'b0000000
`define FUNC7_SLT				7'b0000000
`define FUNC7_SLTU			7'b0000000
`define FUNC7_XOR				7'b0000000
`define FUNC7_SRL				7'b0000000
`define FUNC7_SRA				7'b0100000
`define FUNC7_OR				7'b0000000
`define FUNC7_AND				7'b0000000
