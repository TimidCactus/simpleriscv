module pc_reg(
	input   wire                    clk,
	input   wire                    rst,

	input   wire[`INSTADDRBUS]      next_ins_addr,

	output  reg[`INSTADDRBUS]       pc
);
	always @ (posedge clk) begin
		if (rst == `RSTENABLE)
            now_ins_addr    <=  `ZEROWORD;
		else
			pc  <=  next_ins_addr;
        end
endmodule
