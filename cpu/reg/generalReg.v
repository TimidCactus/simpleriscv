module generalReg(
	input		wire								clk,
	input		wire								rst,

	input		wire								we,
	input		wire[`REGADDRBUS]		waddr,
	input		wire[`REGBUS]				wdata,

	input		wire[`REGADDRBUS]		raddr1,
	output	reg[`REGBUS]				rdata1,

	input		wire[`REGADDRBUS]		raddr2,
	output	reg[`REGBUS]				rdata2
); 
	reg[`REGBUS] greg[`REGADDRBUS];

	assign greg[0] = `ZEROWORD;

	always @ (posedge clk) begin
		if (rst == `RSTDISABLE  &&  we == `WRITEENABLE  &&  waddr != `ZEROADDR)
			greg[waddr] <= wdata;
	end

	assign rdata1 = greg[raddr1] & {`REGWIDTH{~rst}};
	assign rdata2 = greg[raddr2] & {`REGWIDTH{~rst}};
endmodule
