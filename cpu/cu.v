module cu(
    input   wire                branch_flag,

    input   wire[`OPBUS]        id_opcode,

    input   wire                if_delayslot,

    input   wire[`REGADDRBUS]   id_s1_addr,
    input   wire[`REGADDRBUS]   id_s2_addr,
    input   wire                ex_we,
    input   wire[`REGADDRBUS]   ex_dest_addr,
    input   wire                mem_we,
    input   wire[`REGADDRBUS]   mem_dest_addr,

    input   wire[`OPBUS]        mem_opcode,

    output  reg                 branch_mul,

    output  reg                 if_delayslot,

    output  reg                 id_we,

    output  reg[1:0]            id_operand1_mux,
    output  reg[1:0]            id_operand1_mux,

    output  reg                 mem_data_mux

);

    assign  branch_mul      <= branch_flag;
    assign  if_delayslot    <= branch_flag;

    always @ (*) begin
        if ( ( id_opcode == `OP_OP || id_opcode == `OP_OP_IMM || id_opcode == `OP_LUI || id_opcode == `OP_AUIPC || id_opcode == `OP_JAL || id_opcode == `OP_JALR || id_opcode == `OP_LOAD ) && if_delayslot `NOTDELAYSLOT) begin
            id_we <= 1'b1;
        end else begin
            id_we <= 1'b0;
        end
    end

    always @ (*) begin
        if( mem_we == 1'b1 && mem_dest_addr == id_s1_addr) begin
            id_operand1_mux <=  2'b11;
        end else if(ex_we == 1'b1 && ex_dest_addr == id_s1_addr) begin
            id_operand1_mux <=  2'b10;
        end else begin
            id_operand1_mux <=  2'b00;
        end

        if( mem_we == 1'b1 && mem_dest_addr == id_s2_addr) begin
            id_operand2_mux <=  2'b11;
        end else if(ex_we == 1'b1 && ex_dest_addr == id_s2_addr) begin
            id_operand2_mux <=  2'b10;
        end else begin
            id_operand2_mux <=  2'b00;
        end
    end

    always @ (*) begin
        if(mem_opcode == `OP_LOAD) begin
            mem_data_mux <= 1'b1;
        end else begin
            mem_data_mux <= 1'b0;
        end
    end

endmodule
